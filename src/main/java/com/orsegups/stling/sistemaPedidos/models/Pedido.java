package com.orsegups.stling.sistemaPedidos.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
public class Pedido implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@ManyToOne
	@JoinColumn(name = "usuario_id", nullable = false)
	private Usuario vendedor;
	
	@ManyToOne
	@JoinColumn(name = "cliente_id", nullable = false)
	private Cliente cliente;
	
	@Column(nullable = false)
	private Calendar dtCadastro;
	
	@Column(nullable = false)
	private Calendar dtEmissao;
	
	@Column
	private Calendar dtFaturamento;
	
	@Transient
	private List<PedidoProduto> itens;
	
	@Column(nullable = false)
	private BigDecimal valorTotal;
	
	@Column
	private boolean finalizado;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Usuario getVendedor() {
		return vendedor;
	}

	public void setVendedor(Usuario vendedor) {
		this.vendedor = vendedor;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Calendar getDtCadastro() {
		return dtCadastro;
	}

	public void setDtCadastro(Calendar dtCadastro) {
		this.dtCadastro = dtCadastro;
	}

	public Calendar getDtEmissao() {
		return dtEmissao;
	}

	public void setDtEmissao(Calendar dtEmissao) {
		this.dtEmissao = dtEmissao;
	}

	public Calendar getDtFaturamento() {
		return dtFaturamento;
	}

	public void setDtFaturamento(Calendar dtFaturamento) {
		this.dtFaturamento = dtFaturamento;
	}

	public List<PedidoProduto> getItens() {
		return itens;
	}

	public void setItens(List<PedidoProduto> itens) {
		this.itens = itens;
	}

	public BigDecimal getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(BigDecimal valorTotal) {
		this.valorTotal = valorTotal;
	}

	public boolean isFinalizado() {
		return finalizado;
	}

	public void setFinalizado(boolean finalizado) {
		this.finalizado = finalizado;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
}
