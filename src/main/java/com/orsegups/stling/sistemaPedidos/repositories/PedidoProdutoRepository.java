package com.orsegups.stling.sistemaPedidos.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.orsegups.stling.sistemaPedidos.models.PedidoProduto;

public interface PedidoProdutoRepository extends JpaRepository<PedidoProduto, String> {

}
