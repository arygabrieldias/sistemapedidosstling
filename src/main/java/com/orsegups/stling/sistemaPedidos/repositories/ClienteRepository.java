package com.orsegups.stling.sistemaPedidos.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.orsegups.stling.sistemaPedidos.models.Cliente;

public interface ClienteRepository extends JpaRepository<Cliente, String> {

}
