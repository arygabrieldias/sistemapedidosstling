package com.orsegups.stling.sistemaPedidos.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.orsegups.stling.sistemaPedidos.models.TipoUsuario;

public interface TipoUsuarioRepository extends JpaRepository<TipoUsuario, String> {

}
