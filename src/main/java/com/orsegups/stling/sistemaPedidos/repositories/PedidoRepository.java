package com.orsegups.stling.sistemaPedidos.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.orsegups.stling.sistemaPedidos.models.Pedido;

public interface PedidoRepository extends JpaRepository<Pedido, String> {

}
