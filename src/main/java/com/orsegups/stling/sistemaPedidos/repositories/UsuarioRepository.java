package com.orsegups.stling.sistemaPedidos.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.orsegups.stling.sistemaPedidos.models.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario, String> {

}
