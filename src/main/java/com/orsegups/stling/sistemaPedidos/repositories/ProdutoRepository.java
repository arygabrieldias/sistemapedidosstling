package com.orsegups.stling.sistemaPedidos.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.orsegups.stling.sistemaPedidos.models.Produto;

public interface ProdutoRepository extends JpaRepository<Produto, String> {

}
