package com.orsegups.stling.sistemaPedidos.resources;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.orsegups.stling.sistemaPedidos.models.TipoUsuario;
import com.orsegups.stling.sistemaPedidos.repositories.TipoUsuarioRepository;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value="API REST Sistema de Pedidos Stling")
@RestController
@RequestMapping("/pedidosStling/tipoUsuario")
public class TipoUsuarioResource {

	@Autowired
	private TipoUsuarioRepository tur;
	
	@ApiOperation(value="Adicionar TipoUsuario.")
	@PostMapping()
	public TipoUsuario adicionaTipoUsuario(@RequestBody @Valid TipoUsuario tipoUsuario) {
		return tur.save(tipoUsuario);
	}
	
	@ApiOperation(value="Listar TipoUsuarios.")
	@GetMapping(produces="application/json")
	public @ResponseBody List<TipoUsuario> listaTiposUsuario() {
		
		List<TipoUsuario> listaTiposUsuario = tur.findAll();
		
		return listaTiposUsuario;	
	} 
}
