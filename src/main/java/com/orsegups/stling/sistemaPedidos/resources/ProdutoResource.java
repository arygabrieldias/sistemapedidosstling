package com.orsegups.stling.sistemaPedidos.resources;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.orsegups.stling.sistemaPedidos.models.Produto;
import com.orsegups.stling.sistemaPedidos.repositories.ProdutoRepository;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value="API REST Sistema de Pedidos Stling")
@RestController
@RequestMapping("/pedidosStling/produto")
public class ProdutoResource {

	@Autowired
	private ProdutoRepository pr;
	
	@ApiOperation(value="Adicionar Produto.")
	@PostMapping()
	public Produto adicionaProduto(@RequestBody @Valid Produto produto) {
		return pr.save(produto);
	}
	
	@ApiOperation(value="Listar Produtos.")
	@GetMapping(produces="application/json")
	public @ResponseBody List<Produto> listaProdutos() {
		
		List<Produto> listaProdutos = pr.findAll();
		
		return listaProdutos;	
	}
	
	@ApiOperation(value="Atualizar Produto.")
	@PutMapping()
	public Produto updateProduto(@RequestBody @Valid Produto produto) {
		return pr.save(produto);
	}
	
	@ApiOperation(value="Deletar Produto.")
	@DeleteMapping()
	public Produto deletaProduto(@RequestBody @Valid Produto produto) {
		pr.delete(produto);
		return produto;
	}
}
