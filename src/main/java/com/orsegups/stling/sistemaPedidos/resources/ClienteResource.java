package com.orsegups.stling.sistemaPedidos.resources;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.orsegups.stling.sistemaPedidos.models.Cliente;
import com.orsegups.stling.sistemaPedidos.repositories.ClienteRepository;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value="API REST Sistema de Pedidos Stling")
@RestController
@RequestMapping("/pedidosStling/cliente")
public class ClienteResource {
	
	@Autowired
	private ClienteRepository cr;
	
	@ApiOperation(value="Adicionar Cliente.")
	@PostMapping()
	public Cliente adicionaCliente(@RequestBody @Valid Cliente cliente) {
		return cr.save(cliente);
	}
	
	@ApiOperation(value="Listar Clientes.")
	@GetMapping(produces="application/json")
	public @ResponseBody List<Cliente> listaClientes() {
		
		List<Cliente> listaClientes = cr.findAll();
		
		return listaClientes;	
	}
	
	@ApiOperation(value="Atualizar Cliente.")
	@PutMapping()
	public Cliente updateCliente(@RequestBody @Valid Cliente cliente) {
		return cr.save(cliente);
	}
	
	@ApiOperation(value="Deletar Cliente.")
	@DeleteMapping()
	public Cliente deletaCliente(@RequestBody @Valid Cliente cliente) {
		cr.delete(cliente);
		return cliente;
	}

}
