package com.orsegups.stling.sistemaPedidos.resources;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.orsegups.stling.sistemaPedidos.models.Usuario;
import com.orsegups.stling.sistemaPedidos.repositories.UsuarioRepository;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value="API REST Sistema de Pedidos Stling")
@RestController
@RequestMapping("/pedidosStling/usuario")
public class UsuarioResource {
	
	@Autowired
	private UsuarioRepository ur;
	
	@ApiOperation(value="Adicionar Usuario.")
	@PostMapping()
	public Usuario adicionaTipoUsuario(@RequestBody @Valid Usuario usuario) {
		return ur.save(usuario);
	}
	
	@ApiOperation(value="Listar Usuarios.")
	@GetMapping(produces="application/json")
	public @ResponseBody List<Usuario> listaUsuarios() {
		
		List<Usuario> listaUsuarios = ur.findAll();
		
		return listaUsuarios;	
	}

}
