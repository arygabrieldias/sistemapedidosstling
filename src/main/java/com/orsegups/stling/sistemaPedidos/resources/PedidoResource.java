package com.orsegups.stling.sistemaPedidos.resources;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.orsegups.stling.sistemaPedidos.models.Pedido;
import com.orsegups.stling.sistemaPedidos.repositories.PedidoRepository;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value="API REST Sistema de Pedidos Stling")
@RestController
@RequestMapping("/pedidosStling/pedidos")
public class PedidoResource {

	@Autowired
	private PedidoRepository pr;
	
	@ApiOperation(value="Adicionar Pedido.")
	@PostMapping()
	public Pedido adicionaPedido(@RequestBody @Valid Pedido pedido) {
		return pr.save(pedido);
	}
	
	@ApiOperation(value="Listar Pedidos.")
	@GetMapping(produces="application/json")
	public @ResponseBody List<Pedido> listaPedidos() {
		
		List<Pedido> listaPedidos = pr.findAll();
		
		return listaPedidos;	
	}
	
	@ApiOperation(value="Atualizar Pedido.")
	@PutMapping()
	public Pedido updatePedido(@RequestBody @Valid Pedido pedido) {
		return pr.save(pedido);
	}
	
	@ApiOperation(value="Deletar Pedido.")
	@DeleteMapping()
	public Pedido deletaPedido(@RequestBody @Valid Pedido pedido) {
		pr.delete(pedido);
		return pedido;
	}
}
